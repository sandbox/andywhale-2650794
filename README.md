#User Import Date

Plugin for the User Import module, for handling importing date fields from csv.

Uses strtotime to import dates and convert them to the format allocated in the USER_IMPORT_DATE_FORMAT constant.

Additionally the ALLOW_SLASHES_AS_DASHES constant allows for the US format (dd/mm/yyyy) to be converted to the UK format (dd-mm-yyyy) prior to being run through strtotime.

##Is it re-usable?

Yes.